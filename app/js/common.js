$(document).ready( function() {
	$(".header__lenguage_selected").click( function() {
		$(".header__dropdown_ul").toggleClass("active");
	});

	$(".main__checkbox").click( function() {
		$(this).toggleClass("active");
	});
});

function validateEmail(Email) {
	var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

	return $.trim(Email).match(pattern) ? true : false;
}

$("#email").on("keyup", function(){
	if( validateEmail($("#email").val())  == true ) {
		$(this).addClass("ok");
	} else {
		$(this).removeClass("ok");
	}
});

$("#password").on("keyup", function(){
	if( $("#password").val().length > 5 ) {
		$(this).addClass("ok");
	} else {
		$(this).removeClass("ok");
	}
});

$(".button-login").click( function(e) {
	e.preventDefault();

	if( ( validateEmail($("#email").val())  == true ) && ( $("#password").val().length > 5 ) && ( $("#privacy-policy").prop("checked") == true ) && ( $("#trems-and-conditions").prop("checked") == true ) && ( $("#whitepaper").prop("checked") == true )) {
		$("#login_form").submit();
	}

	var inputId;

	function isChecked(inputId) {
		if( $("#" + inputId).prop("checked") == false ) {
			$("#" + inputId).addClass("no-check");
		}
	}

	isChecked("privacy-policy");
	isChecked("trems-and-conditions");
	isChecked("whitepaper");

	if (validateEmail($("#email").val())  == false) {
		$("#email").css("border-color", "#f3ca4c");
	}

	if ($("#password").val().length <= 5) {
		$("#password").css("border-color", "#f3ca4c");
	}

});

